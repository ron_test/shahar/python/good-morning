time = input("Enter the time in hh.mm format\n")

hour = int(time[:2])
minute = int(time[3:5])

if 8 <= hour < 11:
    print("Good morning!")
elif hour == 12 and minute == 0:
    print("Time to eat!")
elif 11 <= hour < 18:
    print("Good afternoon!")
elif 18 <= hour < 22:
    print("Good evening!")
elif 23 <= hour < 24 or 0 <= hour < 6:
    print("It's time for tash!")
